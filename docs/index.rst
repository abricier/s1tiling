.. S1Tiling documentation master file, created by
   sphinx-quickstart on Mon Sep 14 12:38:14 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. role:: raw-html(raw)
    :format: html

S1Tiling -- documentation!
====================================

.. :raw-html:`<a class="reference external image-reference" href="https://badge.fury.io/py/s1tiling" rel="nofollow"><img src="https://badge.fury.io/py/s1tiling.svg" type="image/svg+xml"></a>`
.. :raw-html:`<a class="reference external image-reference" href="https://s1tiling.readthedocs.io/en/latest/" rel="nofollow"><img alt="https://readthedocs.org/projects/pip/badge/?version=latest&amp;style=flat" src="https://readthedocs.org/projects/pip/badge/?version=latest&amp;style=flat"></a>`
.. :raw-html:`<br>`

:raw-html:`<a class="reference external image-reference" href="https://pypi.org/project/s1tiling/" rel="nofollow"><img src="https://img.shields.io/pypi/l/s1tiling.svg" type="image/svg+xml"></a>`
:raw-html:`<a class="reference external image-reference" href="https://pypi.org/project/s1tiling/" rel="nofollow"><img src="https://img.shields.io/pypi/pyversions/s1tiling.svg" type="image/svg+xml"></a>`


On demand Ortho-rectification of Sentinel-1 data on Sentinel-2 grid.

.. toctree::
   :maxdepth: 3
   :caption: User Guide:

   intro
   install
   use
   dataflow
   files
   HAL

.. toctree::
   :maxdepth: 3
   :caption: Developer Documentation:

   contribute
   developers
   testing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
